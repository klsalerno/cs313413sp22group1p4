package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class IncrementingState implements StopwatchState {

    public IncrementingState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    int threeCount = 0; // Initialize 3-second counter to 0

    @Override
    public void onStartStop() {
        sm.actionInc(); // Increment runtime by 1
        threeCount = 0; // Reset 3-second counter to 0 every time the button is pressed
        if (sm.getRuntime() == 99) {
            sm.actionBeep(); // Beep once
            sm.toRunningState(); // Doesn't take you to the state until method completes --> need if-else
        } else {
            sm.toIncrementingState();
        }

    }


    @Override
    public void onTick() {
        if (threeCount == 3) { // 3 seconds pass from last button press
            sm.actionBeep(); // Beep once
            sm.toRunningState();
        } else {
            threeCount++; // Increment 3-second counter every tick event
            sm.toIncrementingState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREMENTING;
    }
}
